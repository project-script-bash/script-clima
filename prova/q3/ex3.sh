#!/bin/bash


echo "É possível criar uma variavel escrevendo por atribuição direta 
a = x 

chamando por exemplo "read y"
uma variavel y vai ser criada quando um usuário digitar alguma coisa.

Existem também atribuíção por cálculo, a variável recebe o resultado
de alguma operação matemática.
resultado=$((10 + 5))

"
