#!/bin/bash

echo "lista com 20 variáveis automáticas"
echo ""
echo '1. $HOME --> O diretório home do usuário atual -' $HOME

echo '2. $USER --> O nome de usuário do usuário atual -' $USER

echo '3. $PATH --> Uma lista de diretórios onde o sistema procura por comandos executáveis - '$PATH

echo '4. $SHELL --> O shell padrão do usuário atual - '$SHELL

echo '5. $PWD --> O diretório de trabalho atual - '$PWD

echo '6. $HOSTNAME --> O nome do host do sistema - '$HOSTNAME

echo '7. $UID --> O ID do usuário atual - '$UID

echo '8. $TERM --> O tipo de terminal atual - '$TERM

echo '9. $LANG --> A configuração de idioma e localização do sistema - '$LANG

echo '10. $SUDO_USER --> O nome de usuário do superusuário, quando executado com sudo. - '$SUDO_USER

echo '11. $DISPLAY --> A exibição gráfica atual - '$DISPLAY

echo '13. $EDITOR --> O editor de texto padrão atual - '$EDITOR

echo '14. $SSH_CLIENT --> informações sobre o cliente SSH - '$SSH_CLIENT

echo '14. $TZ --> Configuração de fuso horário - '$TZ

echo '15. $OSTYPE --> O tipo de sistema operacional - '$OSTYPE

echo '16. $HISTFILE --> O arquivo onde o histórico de comandos é armazenado - '$HISTFILE

echo '17. $HISTSIZE --> O número máximo de comandos no histórico - '$HISTSIZE

echo '18. $PS1 --> O prompt de comando primário - ' $PS1

echo '19. $PS2 --> O prompt de comando secundário - '$PS2

echo '20. $IFS --> Separador de compo interno, usado para dividir palavras em variáveis de texto - '$IFS


